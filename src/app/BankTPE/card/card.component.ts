import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
	showMessageSuccess: boolean;
	showMessageFailed: boolean;
  showMessageDelete: boolean;
	submitted: boolean;

	carte: any;

  constructor(private serviceCarte:ServiceService) { }

  ngOnInit() {
  	this.ajouterTabCarte();
  }

	onSubmit(){
		
	this.submitted = true;
	if (this.serviceCarte.formCarte.valid) {
        this.serviceCarte.AjouterCarte()
          .subscribe(
            data => {
              this.showMessageSuccess = true;
              setTimeout(() => this.showMessageSuccess = false, 3000);
              console.log("SUCCESS ", data);
              this.serviceCarte.formCarte.reset();
               window.location.reload();
 
            }, err => {
                this.showMessageFailed = true;
                setTimeout(() => this.showMessageFailed = false, 3000);
                console.log("Error")
              }
          )
        console.log('OK !');
      }
    }

    ajouterTabCarte(){
    	this.serviceCarte.getCarte()
    	.subscribe(
    		(res:any)=>{
    			this.carte = res;
    			console.log(res)
    		}
    		)
    }
    supprimerCarte(id){
       if(this.serviceCarte.formCarte.removeControl){
         this.serviceCarte.delCard(id)
         .subscribe(
              data => {
              this.showMessageDelete = true;
              setTimeout(() => this.showMessageDelete = false, 3000);
              console.log("SUCCESS ", data);
               window.location.reload();
 
            }, err => {
                this.showMessageFailed=true;
                setTimeout(() => this.showMessageFailed = false, 4000);
                console.log("Error")
              }
          )
     }
    }

}

