import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './BankTPE/register/register.component';
import { CardComponent } from './BankTPE/card/card.component';
import { TpeComponent } from './BankTPE/tpe/tpe.component';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BorneDeServiceComponent } from './BankTPE/borne-de-service/borne-de-service.component';
import { AddTerminalComponent } from './BankTPE/add-terminal/add-terminal.component';
import { BorneServiceComponent } from './BankTPE/borne-service/borne-service.component';
import { GestionUtilisateurComponent } from './BankTPE/gestion-utilisateur/gestion-utilisateur.component';
import { ModificationPersonnelComponent } from './BankTPE/modification-personnel/modification-personnel.component';
import { GestionProfilComponent } from './BankTPE/gestion-profil/gestion-profil.component'


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    CardComponent,
    TpeComponent,
    BorneDeServiceComponent,
    AddTerminalComponent,
    BorneServiceComponent,
    GestionUtilisateurComponent,
    ModificationPersonnelComponent,
    GestionProfilComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

