import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardComponent } from './BankTPE/card/card.component';
import { TpeComponent } from './BankTPE/tpe/tpe.component';
import { BorneDeServiceComponent } from './BankTPE/borne-de-service/borne-de-service.component'
import { AddTerminalComponent } from './BankTPE/add-terminal/add-terminal.component'
import { GestionUtilisateurComponent } from './BankTPE/gestion-utilisateur/gestion-utilisateur.component'
import { ModificationPersonnelComponent } from './BankTPE/modification-personnel/modification-personnel.component'
import { GestionProfilComponent } from './BankTPE/gestion-profil/gestion-profil.component'

const routes: Routes = [
  {path: 'card', component: CardComponent},
  {path: 'tpe', component: TpeComponent},
  {path: 'borneDeService', component: BorneDeServiceComponent},
  {path: 'addTerminal', component: AddTerminalComponent},
  {path: 'gestionUser', component: GestionUtilisateurComponent},
  {path: 'modifPerso', component: ModificationPersonnelComponent},
  {path: 'gestionProfil', component: GestionProfilComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
