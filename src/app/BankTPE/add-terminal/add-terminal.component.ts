import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { NgForm } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-terminal',
  templateUrl: './add-terminal.component.html',
  styleUrls: ['./add-terminal.component.css']
})
export class AddTerminalComponent implements OnInit {

	formControls = this.serviceTpe.formTPE.controls;
	showMessageSuccess: boolean;
	showMessageFailed: boolean;
	submitted: boolean;
	showDeleteMessage: boolean;

  tpe: any;
  imei: any;
  mac: any;
  type: any;
  status: any;

  constructor(private serviceTpe: ServiceService, private rooter: Router, private http: HttpClient) { }

  ngOnInit() {
  	this.ajouterTabTpe();
  }
 	onSubmit(){
	this.submitted = true;
	if (this.serviceTpe.formTPE.valid) {
        this.serviceTpe.AjouterTPE()
          .subscribe(
            data => {

              //triage (TPE ou Borne de service)
              console.log(this.serviceTpe.formTPE.get('type').value)
              if(this.serviceTpe.formTPE.get('type').value =='TPE'){
                  this.rooter.navigate(['tpe']);
              }else{
                this.rooter.navigate(['borneDeService']);
              }
          
              this.showMessageSuccess = true;
              setTimeout(() => this.showMessageSuccess = false, 3000);
              console.log("SUCCESS ", data);
              this.serviceTpe.formTPE.reset();
              
 
            }, err => {
                this.showMessageFailed = true;
                setTimeout(() => this.showMessageFailed = false, 3000);
                console.log("Error")
              }
          )
        console.log('OK !');
      }
    }

    ajouterTabTpe(){
    	this.serviceTpe.getTpe()
    	.subscribe(
    		(res:any)=>{
    			this.tpe = res;
    			console.log(res)
    		}
    	)
	 }

test( i ){   
  console.log(this.tpe.data[i].status)
  console.log("http://192.168.1.250/dev/bankcard/web/api/tpe/"+this.tpe.data[i].id+"/update")
}
}
