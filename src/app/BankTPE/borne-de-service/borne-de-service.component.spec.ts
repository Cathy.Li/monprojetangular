import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorneDeServiceComponent } from './borne-de-service.component';

describe('BorneDeServiceComponent', () => {
  let component: BorneDeServiceComponent;
  let fixture: ComponentFixture<BorneDeServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorneDeServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorneDeServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
