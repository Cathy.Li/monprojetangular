import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { NgForm } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-borne-de-service',
  templateUrl: './borne-de-service.component.html',
  styleUrls: ['./borne-de-service.component.css']
})
export class BorneDeServiceComponent implements OnInit {

  formControls = this.serviceTpe.formTPE.controls;
	showMessageSuccess: boolean;
	showMessageFailed: boolean;
	submitted: boolean;
	showDeleteMessage: boolean;

	tpe: any;
  imei: any;
  mac: any;
  type: any;
  status: any;

  etat: any=[];
  searchText: string='Borne de service'

  constructor(private serviceTpe: ServiceService, private rooter: Router, private http: HttpClient) { }

  ngOnInit() {
  	this.ajouterTabTpe();
  }

    filterCondition(typ: any) {     
      return typ.type.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
    }


 	onSubmit(){
	this.submitted = true;
	if (this.serviceTpe.formTPE.valid) {
        this.serviceTpe.AjouterTPE()
          .subscribe(
            data => {
              this.showMessageSuccess = true;
              setTimeout(() => this.showMessageSuccess = false, 3000);
              console.log("SUCCESS ", data);
              this.serviceTpe.formTPE.reset();
              
 
            }, err => {
                this.showMessageFailed = true;
                setTimeout(() => this.showMessageFailed = false, 3000);
                console.log("Error")
              }
          )
        console.log('OK !');
      }
    }

    ajouterTabTpe(){
    	this.serviceTpe.getTpe()
    	.subscribe(
    		(res:any)=>{
    			this.tpe = res;
    			console.log(res)
    		}
    	)
	 }

   supprimer(id){
     if(this.serviceTpe.formTPE.removeControl){
         this.serviceTpe.DeleteTPE(id)
         .subscribe(
              data => {
              this.showMessageSuccess = true;
              setTimeout(() => this.showDeleteMessage = false, 3000);
              console.log("SUCCESS ", data);
 
            }, err => {
                this.showMessageFailed=true;
                setTimeout(() => this.showMessageFailed = false, 4000);
                console.log("Erreur")
              }
          )
     }
   }
   
test( i ){   
  console.log(this.tpe.data[i].status)
  console.log("http://192.168.1.250/dev/bankcard/web/api/tpe/"+this.tpe.data[i].id+"/update")
}

   formPut = new FormGroup({
      imei: new FormControl('', Validators.required),
      mac: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required)
     
   }) 
   
  putUpdate(i){
    //recup id
    var id = this.tpe.data[i].id
    console.log(id)
  }

getStatus(id){
  this.serviceTpe.status(id)
  .subscribe(
        (res:any)=>{
          this.etat = [res.data];
          res= this.etat
          console.log("test")
          console.log(res)
        }
      )
}

  actualiserTpe(id){
     this.serviceTpe.updateTpe(id)
     .subscribe(
         (res:any)=>{
           console.log(res)
         },
         err =>{
           console.log("error")
         }
       )
  }
}
