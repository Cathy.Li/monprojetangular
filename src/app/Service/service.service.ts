import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private getTPE = "http://192.168.1.250/dev/bankcard/web/api/tpe";
  private deleteTPE = "http://192.168.1.250/dev/bankcard/web/api/tpe/";
  private addTPE = "http://192.168.1.250/dev/bankcard/web/api/tpe/add";

  private getCard = "http://192.168.1.250/dev/bankcard/web/api/card";
  private addCarte = "http://192.168.1.250/dev/bankcard/web/api/card/add";
  private deleteCarte = "http://192.168.1.250/dev/bankcard/web/api/card/id";

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

data:any=[];
//getTPE
  getTpe() {
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'X-AUTH-TOKEN': 'bankcardfintek'
    });
 console.log(headers)
    return this.http.get(this.getTPE,{ headers: headers })
  }

   formTPE = new FormGroup({
    
    imei: new FormControl('', Validators.required),
    mac: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required)

  })

   formPut = new FormGroup({
      imei: new FormControl('', Validators.required),
      mac: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required)
     
   })
    AjouterTPE(){

    var formData = new FormData();

     formData.append("imei",this.formTPE.get('imei').value )
     formData.append("mac",this.formTPE.get('mac').value )
     formData.append("type",this.formTPE.get('type').value )
     formData.append("status",this.formTPE.get('status').value )
     

   var hea = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'X-AUTH-TOKEN': 'bankcardfintek'
    });

	
	console.log(formData)
   return this.http.post(this.addTPE, formData, {headers: hea})   
  }

//getCarte
  getCarte() {
    var hea = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json', 	
      'X-AUTH-TOKEN': 'bankcardfintek'
    });
 console.log(hea)
    return this.http.get(this.getCard,{ headers: hea })
  }
 
//Carte
formCarte = new FormGroup({
    
    cardNumber: new FormControl('', Validators.required),
    pin: new FormControl('', Validators.required),
    identifiant: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  })


//ajouter Carte
  AjouterCarte(){

      var formData = new FormData();

     formData.append("cardNumber",this.formCarte.get('cardNumber').value )
     formData.append("pin",this.formCarte.get('pin').value )
     formData.append("identifiant",this.formCarte.get('identifiant').value )
     formData.append("phone",this.formCarte.get('phone').value )
     formData.append("password",this.formCarte.get('password').value )


    var reqHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',   
      'X-AUTH-TOKEN': 'bankcardfintek'
    });
    console.log(reqHeaders)
    return this.http.post(this.addCarte, formData, { headers: reqHeaders })
  }

//Supprimer TPE
  DeleteTPE(id){
    var req = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'X-AUTH-TOKEN': 'bankcardfintek'
 
    });
    console.log(req)
    return this.http.delete("http://192.168.1.250/dev/bankcard/web/api/tpe/"+id,{ headers: req })
  } 

  //supprimer carte

   delCard(id){
    var req = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'X-AUTH-TOKEN': 'bankcardfintek'
 
    });
    console.log(req)
    return this.http.delete("http://192.168.1.250/dev/bankcard/web/api/card/"+id,{ headers: req })
  } 

//UPDATE TPE
  updateTpe(id){

   var data = ({
      "data":[{
        "imei": this.formPut.get('imei').value,
        "mac": this.formPut.get('mac').value,
        "type": this.formPut.get('type').value,
        "status": this.formPut.get('status').value
      }]
    });

   var heatpe = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'X-AUTH-TOKEN': 'bankcardfintek'
    });

  let url = "http://192.168.1.250/dev/bankcard/web/api/tpe/"+id+"/update"
  console.log(url)

   return this.http.put("http://192.168.1.250/dev/bankcard/web/api/tpe/"+id+"/update", data, {headers: heatpe}) 
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  status(id){
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'X-AUTH-TOKEN': 'bankcardfintek'
    });
 console.log(headers)
    return this.http.get(this.getTPE+"/"+id,{ headers: headers })

  }

  modifiertpe(id){
    var data = ({
      "data":[{
        "imei": this.formPut.get('imei').value,
        "mac": this.formPut.get('mac').value,
        "type": this.formPut.get('type').value,
        "status": this.formPut.get('status').value
      }]
    });

    var head = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'Content-type': 'application/x-www-form-urlencoded',
      'X-AUTH-TOKEN': 'bankcardfintek'
    })

    console.log("http://192.168.1.250/dev/bankcard/web/api/tpe/"+id+"/update")
    return this.http.put("http://192.168.1.250/dev/bankcard/web/api/tpe/"+id+"/update", data, {headers: head}) 
 }
  
}
  
