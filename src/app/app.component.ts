import { Component } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BankCard & TPE';
  showComponent:boolean=false;

  username: string = 'admin';
  password: string = '1234';


  constructor(private service: ServiceService, private rooter: Router){

  }
  	onLogout(){
    this.service.deleteToken();
    location.reload();
    this.showComponent=false;

  }
  login(username,password){
    //this.showComponent=true;
     if(username === this.username && password === this.password){
     this.showComponent=true;
    }else {
      alert("ERREUR: Verifier votre Identifiant ou mot de passe");
      this.showComponent=false;
    }
  }
}
