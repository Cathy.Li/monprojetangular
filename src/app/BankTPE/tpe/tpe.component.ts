import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { NgForm } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-tpe',
  templateUrl: './tpe.component.html',
  styleUrls: ['./tpe.component.css']
})
export class TpeComponent implements OnInit {
	formControls = this.serviceTpe.formTPE.controls;
	showMessageSuccessTPE: boolean;
	showMessageFailedTPE: boolean;
  showDeleteMessageTPE: boolean;
	submitted: boolean;
	showDeleteMessage: boolean;

  //update url
	tpe: any;

  imei: any;
  mac: any;
  type: any;
  etat: any=[];

  searchText: string='TPE'

  constructor(private serviceTpe: ServiceService, private rooter: Router, private http: HttpClient) { }

  ngOnInit() {
  	this.ajouterTabTpe();
  }

   filterCondition(typa: any) {     
      return typa.type.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
    }

 	onSubmit(){
	this.submitted = true;
	if (this.serviceTpe.formTPE.valid) {
        this.serviceTpe.AjouterTPE()
          .subscribe(
            data => {
              this.showMessageSuccessTPE = true;
              setTimeout(() => this.showMessageSuccessTPE = false, 3000);
              console.log("SUCCESS ", data);
              this.serviceTpe.formTPE.reset();           
 
            }, err => {
                this.showMessageFailedTPE = true;
                setTimeout(() => this.showMessageFailedTPE = false, 3000);
                console.log("Error")
              }
          )
        console.log('OK !');
      }
    }

    ajouterTabTpe(){
    	this.serviceTpe.getTpe()
    	.subscribe(
    		(res:any)=>{
    			this.tpe = res;
    			console.log(res)
    		}
    	)
	 }

   supprimer(id){
     if(this.serviceTpe.formTPE.removeControl){
         this.serviceTpe.DeleteTPE(id)
         .subscribe(
              data => {
              this.showMessageSuccessTPE = true;
              setTimeout(() => this.showDeleteMessage = false, 3000);
              console.log("SUCCESS ", data);
              //window.location.reload();
              //location.reload()
 
            }, err => {
                this.showMessageFailedTPE=true;
                setTimeout(() => this.showMessageFailedTPE = false, 4000);
                console.log("Erreur")
              }
          )
     }
   }


test( i ){   
  console.log(this.tpe.data[i].status)
  console.log("http://192.168.1.250/dev/bankcard/web/api/tpe/"+this.tpe.data[i].id+"/update")
}
  Activer(id){
      console.log("oui")
     //var i = this.tpe.data[id].status
     if(this.serviceTpe.formPut.valid){
       this.serviceTpe.modifiertpe(status)
       .subscribe(
          data => {
              this.showMessageSuccessTPE = true;
              console.log("SUCCESS ", data);
              console.log(this.serviceTpe.formPut.value)
               //window.location.reload();
               //location.reload()
            }, err => {
                //this.showMessageFailed=true;
                setTimeout(() => this.showDeleteMessageTPE = false, 3000);
                console.log("Error")
              }
         )
     }
   }

  putUpdate(i){

    //recup id
    var id = this.tpe.data[i].id
    console.log(id)
  }

getStatus(id){
  this.serviceTpe.status(id)
  .subscribe(
        (res:any)=>{
          this.etat = [res.data];
          res= this.etat
          console.log("test")
          console.log(res)
        }
      )
}

  actualiserTpe(id){
     this.serviceTpe.updateTpe(id)
     .subscribe(
         (res:any)=>{
           console.log(res)
         },
         err =>{
           console.log("error")
         }
       )
  }


}







