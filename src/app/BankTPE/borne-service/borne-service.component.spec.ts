import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorneServiceComponent } from './borne-service.component';

describe('BorneServiceComponent', () => {
  let component: BorneServiceComponent;
  let fixture: ComponentFixture<BorneServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorneServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorneServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
